<articel>
        <!-- Home -->
        <header class="text-white text-center ">
                <div class="bg-head " style="background-image:url('https://images.unsplash.com/photo-1423784346385-c1d4dac9893a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=558f2d8bcbb75255175c16b662804de8&auto=format&fit=crop&w=1350&q=80');">

                        <h1 class="font-weight-bold display-4">คนขายได้ขายของ คนซื้อได้ของ</h1>
                        <h2 class='text-center font-weight-bold'>หมดกังวลเรื่องการโดนโกง ให้เราเป็นตัวช่วย</h2>
                        <div class="botton mt-5 ">สมัครสมาชิก</div>

                </div>
        </header>
        <!-- section โล่ -->
  <!-- <section class="text-center p-4" style="background-color: #F3F3F3;">
                <h3 class="font-weight-bold">ระบบกลางถือเงินเพื่อการซื้อขายที่ปลอดภัย</h3>
                <h4>ป้องกันการโกงเพื่อผลประโยชน์ของท่าน</h4>
                <i class="fas fa-shield-alt fa-3x" style="color: #317FF6"></i>
  </section> -->
  
        <p class="text-center p-1" style="background-color: #317FF6; color: white;">ประกาศจากทีมงาน : ผ่านกลางกับเราฟรีค่าผ่านกลางตั้งแต่ วันนี้ - 30 พฤษภาคม 2561</p>
<!-- section sponser -->
<section class=" mb-3" >
        <div class="container text-center">
        <div class="row">
                <img class="icon-sponser" src="https://images.unsplash.com/photo-1505489435671-80a165c60816?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=af3fb5171edc0c520c99c8061510f4b8&auto=format&fit=crop&w=1062&q=80" alt="">
                <img class="icon-sponser" src="https://images.unsplash.com/photo-1522139137660-4248e04955b8?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=97722da904c360121f0ee15dcd13b1c4&auto=format&fit=crop&w=1355&q=80" alt="">
                <img class="icon-sponser" src="https://images.unsplash.com/photo-1496200186974-4293800e2c20?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=4538d62f60afac8c15ca77b7c57603ce&auto=format&fit=crop&w=1489&q=80" alt="">
                <img class="icon-sponser" src="http://athletehome-eg.com/image/cache/catalog/Brands/Sponser_Logo_Pantone_horizontal-600x315.png" alt="">
        </div>
        </div>
        </section>


        <section style="background-color: #F3F3F3;">
        <div class="container p-5">
                        <div class="text-center">
                                <h2 class="font-weight-bold">เราคือเว็ปซื้อ-ขายผ่านระบบตัวกลาง</h2>
                                <h5>เว็ปของเราเหมาะกับใคร?</h5>
                                <hr class="org">
                        </div>
        </div>   
               </section>

               



        <!-- section ขั้นตอน -->
        <section class="text-center p-5 container-fulid">
                <h3>ขั้นตอนการใช้งาน</h3>
                <span class="h5">ไม่เข้าใจการใช้?<a href="index.php" target="_blank"> ดูวีดีโอ</a></span>
                <hr class="org">
        <div class="row mt-3 step">
         <!-- <img class="bg-img" src="./image/step.PNG" alt="">  -->
         <div class="col"><div class="cricle-service mt-3"> <b><span>1 </span></b></div><br> <h5>สมัครสมาชิก </h5></div>
        <div class="col"><div class="cricle-service mt-3"> <b><span>2 </span></b></div><br> <h5>ทำรายการขาย</h5> <p>ให้ผู้ซื้อโอนเงินเข้าระบบโดยที่จัสบายจะเป็นตัวกลางถือเงินเพื่อความปลอดภัย</p> </div>
        <div class="col"><div class="cricle-service mt-3"> <b><span>3 </span></b></div><br> <h5>ผู้ขายส่งของ</h5> <p>สามารถหมายเลขพัสดุไปตรวจสอบสถานะได้</p> </div>
        <div class="col"><div class="cricle-service mt-3"> <b><span>4 </span></b></div><br> <h5>ผู้ซื้อได้รับของ</h5> <p>หากสินค้าไม่ตรงที่ตกลงกันไว้กรุณาติดต่อผู้ขายและทำรายการคืนของภายใน 7 วัน</p> </div>
        <div class="col"><div class="cricle-service mt-3"> <b><span>5 </span></b></div><br> <h5>รับเงิน</h5> <p>ระบบจะทำการโอนเงินเข้าบัญชีธนาคารของผู้ขายภายใน 7 วันเมื่อผู้ซื้อทำการยืนยัน</p> </div>

         


        </div>
        </div>
        </section>
        
<!-- section บริการของเรา -->
          <section id="services" class="pt-5" style="background-color: #F3F3F3;">
                        <div class="container">
                                <div class="text-center">
                                        <h3>สิ่งที่คุณจะได้รับ </h3>
                                        <hr class="org">
                                </div>
                        <div class="row">
                                <img class="col-md-4 bg-img" src="./image/1.png" alt="">
                                <div class="col-md-8">
                                        <ul class="why mt-5">
                                                <li><span><i class="fas fa-shield-alt"></i></span><h4>ความปลอดภัย</h4><p>ความปลอดภัยเราจะรักษาเงินของผู้ซื้อจนแน่ใจแล้วว่าคุณได้ของที่ถูกต้องตามที่ตกลงกับผู้ขาย</p></li> 
                                                <li><span><i class="fas fa-gavel"></i></span><h4>ความเป็นธรรม</h4><p>เมื่อมีการถูกโกงให้ดำเนินการตามกฏหมายและทางเราจะให้ความเป็นธรรมกับฝ่ายที่ถูก</p></li>
                                                <li><span><i class="fas fa-lock"></i></span><h4>ความเป็นส่วนตัว</h4><p>ข้อมูลของคุณถูกเก็บไว้อย่างปลอดภัยโดยไม่นำไปใช้ประโยชน์อย่างอื่น</p></li>
                                                <li><span><i class="fas fa-hands-helping"></i></span><h4>สินทรัพย์ของคุณ</h4><p>เราจะรักษาเงินของผู้ซื้อจนแน่ใจแล้วว่าคุณได้ของที่ถูกต้องตามที่ตกลงกับผู้ขาย</p></li>
                                                <li><span><i class="fas fa-undo-alt"></i></span><h4>การคืนสินค้า</h4><p>เมื่อคุณได้สินค้าที่ไม่ถูกต้องตามที่ตกลงไว้ คุณสามารถคืนสินค้านั้นได้</p></li>
                                        </ul>
                                </div>
                           </div>
                        </div>
        </section>
        <section class="p-4" style="background-color: #317FF6;">
                        <div class="text-center text-white">
                                      <h3 class="font-weight-bold ">ระบบกลางถือเงินเพื่อการซื้อขายที่ปลอดภัย</h3>
                                      <!-- <i class="fas fa-shield-alt fa-3x"></i> -->
                        </div>
                      </div>
                      </section>
        <!-- section ความเห็น -->
        <section>       
                <div class="bg-comment text-center p-4">  
                        <h3>ความคิดเห็น</h3>
                        <hr class="org">
                        <div class="row">
                        <div class="col-xl-11 mx-auto" data-repuso-grid="6074" data-website-id="0"></div>       
                </div> 
                <h5 class="text-center mt-3">แชร์ความประทับใจของคุณเกี่ยวกับเว็ปไซต์</h5>
                <a data-repuso-collect-modal='team'><button type="button" class="btn btn-primary mt-1"><i class="far fa-comment fa-2x"><span style="font-size:20px;"> ส่งความเห็นของคุณ</span></i> </button></a>
        

        </section>



        <!-- section team -->
        <!-- <section id="team">  
                <div class="team">
                        <div class="container">
                                        <div class="text-center">
                                                <h2><span class="ion-minus"></span> เกี่ยวกับเรา <span class="ion-minus"></span></h2>
                                                <span>บริษัทเรา คือทางเลือกในการซื้อขายออนไลน์ ด้วยระบบความปลอดภัยที่รับประกันว่าคุณจะไม่โดนโกงแน่นอน</span>  <br>
                                                      <span>สามารถซื้อขายได้อย่างสบายใจโดยที่คนขายก็ได้ขายของที่ต้องการและคนซื้อก็อยากซื้อของที่อยากได้ด้วยระบบของเรา</span> </p>
                                                <br>
                                </div>
                                <div class="row text-center">
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                <img class="circle-img" alt="team-photo" src="./image/team.jpg"width="80%">
                                                <div class="team-member">
                                                        <h4>Marutthep Rompho</h4>
                                                        <p>Co-founder, Web designer, UI</p>
                                                </div>
                                                <p class="social">
                                                        <a href="#"> <i class="fab fa-facebook-square fa-2x"></i> </a>
                                                        <a href="#"><i class="fab fa-line fa-2x"></i></a>
                                                        <a href="#"><i class="fas fa-envelope-square fa-2x"></i></a>
                                                </p>
                                        </div>
                                       

                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">

                                                <img class="circle-img" alt="team-photo" src="http://via.placeholder.com/200x200"width="80%">
                                                <div class="team-member">
                                                        <h4>Jack Doe</h4>
                                                        <p>Co-founder,</p>
                                                </div>
                                                <p class="social">
                                                        <a href="#"><i class="fab fa-facebook-square fa-2x"></i> </a>
                                                        <a href="#"><i class="fab fa-line fa-2x"></i></a>
                                                        <a href="#"><i class="fas fa-envelope-square fa-2x"></i></a>
                                                </p>
                                        </div>
                                       
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                <img class="circle-img" alt="team-photo" src="http://via.placeholder.com/200x200"width="80%">
                                                <div class="team-member">
                                                        <h4>James</h4>
                                                        <p>Co-founder,</p>
                                                </div>
                                                <p class="social">
                                                        <a href="#"><i class="fab fa-facebook-square fa-2x"></i> </a>
                                                        <a href="#"><i class="fab fa-line fa-2x"></i></a>
                                                        <a href="#"><i class="fas fa-envelope-square fa-2x"></i></a>
                                                </p>
                                        </div>
                                        

                                       
                                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                                                <img class="circle-img" alt="team-photo" src="http://via.placeholder.com/200x200"width="80%">
                                                <div class="team-member">
                                                        <h4>James</h4>
                                                        <p>Co-founder,</p>
                                                </div>
                                                <p class="social">
                                                        <a href="#"><i class="fab fa-facebook-square fa-2x"></i> </a>
                                                        <a href="#"><i class="fab fa-line fa-2x"></i></a>
                                                        <a href="#"><i class="fas fa-envelope-square fa-2x"></i></a>
                                                </p>
                                        </div>
                                       
                                </div>
                                
                        </div>
                </div>

        
        </section> -->




</articel>